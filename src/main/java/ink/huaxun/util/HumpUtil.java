package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaogang
 * @description 驼峰命名工具类
 * @date 2023/3/15 13:55
 */
public class HumpUtil {

    /**
     * 驼峰转下划线
     */
    public static String underscoreName(String camelCaseName) {
        StringBuilder result = new StringBuilder();
        if (StringUtils.isBlank(camelCaseName)) {
            return StringUtils.EMPTY;
        }
        camelCaseName = StringUtils.uncapitalize(camelCaseName);
        for (int i = 0; i < camelCaseName.length(); i++) {
            char ch = camelCaseName.charAt(i);
            if (Character.isUpperCase(ch)) {
                result.append("_");
                result.append(Character.toLowerCase(ch));
                continue;
            }
            result.append(ch);
        }
        return result.toString();
    }

    /**
     * 下划线转驼峰
     */
    public static String camelCaseName(String underscoreName) {
        StringBuilder result = new StringBuilder();
        if (StringUtils.isBlank(underscoreName)) {
            return StringUtils.EMPTY;
        }
        for (int i = 0; i < underscoreName.length(); i++) {
            char ch = underscoreName.charAt(i);
            if ('_' == ch) {
                result.append(Character.toUpperCase(underscoreName.charAt(i + 1)));
                i++;
                continue;
            }
            result.append(ch);
        }
        return result.toString();
    }

}
