package ink.huaxun.util;

import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;

/**
 * @author zhaogang
 * @description 获取spring中上下文工具类
 * @date 2023/3/15 13:55
 */
public class SpringContextUtil {

    private static ApplicationContext APPLICATION_CONTEXT = null;

    public static void setApplicationContext(@NonNull ApplicationContext applicationContext) {
        if (SpringContextUtil.APPLICATION_CONTEXT == null) {
            SpringContextUtil.APPLICATION_CONTEXT = applicationContext;
        }
    }

    public static <T> T getBean(Class<T> cls) {
        if (APPLICATION_CONTEXT == null) {
            throw new RuntimeException("applicationContext注入失败");
        }
        return APPLICATION_CONTEXT.getBean(cls);
    }

    public static Object getBean(String name) {
        if (APPLICATION_CONTEXT == null) {
            throw new RuntimeException("applicationContext注入失败");
        }
        return APPLICATION_CONTEXT.getBean(name);
    }

    public static <T> T getBean(String name, Class<T> cls) {
        if (APPLICATION_CONTEXT == null) {
            throw new RuntimeException("applicationContext注入失败");
        }
        return APPLICATION_CONTEXT.getBean(name, cls);
    }

}
