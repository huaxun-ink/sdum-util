package ink.huaxun.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author zhaogang
 * @description json操作工具类
 * @date 2023/3/7 16:00
 */
public class JsonUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

    private static final ObjectMapper MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /**
     * json字符串转对象
     */
    public static <T> T parse(String json, Class<T> clazz) {
        if (StringUtils.isBlank(json) || clazz == null) {
            return null;
        }
        try {
            return MAPPER.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            LOGGER.warn(" Parse String to Object error ", e);
            return null;
        }
    }

    /**
     * json字符串转list
     */
    public static <T> List<T> parseArray(String json, Class<T> clazz) {
        if (StringUtils.isBlank(json) || clazz == null) {
            return null;
        }
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, clazz);
        try {
            return MAPPER.readValue(json, javaType);
        } catch (JsonProcessingException e) {
            LOGGER.warn(" Parse String to Array error ", e);
            return null;
        }
    }

    /**
     * 对象转json字符串
     */
    public static <T> String toString(T json) {
        return toString(json, false);
    }

    /**
     * 对象转json字符串
     */
    public static <T> String toString(T json, boolean format) {
        if (json == null) {
            return null;
        }
        try {
            return format ? MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(json) : MAPPER.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            LOGGER.warn(" Parse Object to String error ", e);
            return null;
        }
    }

}
