package ink.huaxun.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

/**
 * @author zhaogang
 * @description 自定义模板工具类
 * @date 2021-06-07 17:15
 */
public class TemplateUtil {

    /**
     * 模板路径
     */
    private static final String TEMPLATE_URL = "\\template";

    /**
     * 模板后缀
     */
    private static final String TEMPLATE_SUFFIX = ".ftl";

    public static void create(Object data, String name, String path) {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        // 设置默认编码
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        // 设置模板所在文件夹
        configuration.setClassForTemplateLoading(TemplateUtil.class, TEMPLATE_URL);
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(new File(path)), StandardCharsets.UTF_8)) {
            Template template = configuration.getTemplate(name + TEMPLATE_SUFFIX);
            template.process(data, writer);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

}
