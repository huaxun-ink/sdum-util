package ink.huaxun.util.alisms;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import ink.huaxun.util.JsonUtil;
import ink.huaxun.util.SpringContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述: 阿里短信工具类
 *
 * @author fan
 * @date 2020-06-22 16:10
 */
public class ALiSmsUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ALiSmsUtil.class);


    private static final Environment ENVIRONMENT = SpringContextUtil.getBean(Environment.class);
    /**
     * 描述: 阿里云账号的accessId
     */
    private static final String ACCESS_KEY_ID = ENVIRONMENT.getProperty("ali-sms.access-key-id");
    /**
     * 描述: 阿里云账号的accessId
     */
    private static final String ACCESS_SECRET = ENVIRONMENT.getProperty("ali-sms.access-secret");


    /**
     * 描述: 产品域名,开发者无需替换
     */
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";
    /**
     * 描述: 阿里短信固定参数，无需修改
     */
    private static final String COMMON_REQUEST_VERSION = "2017-05-25";
    /**
     * 描述: 阿里短信固定参数，无需修改
     */
    private static final String COMMON_REQUEST_ACTION = "SendSms";
    /**
     * 描述: 阿里短信固定参数，无需修改
     */
    private static final String REGION_ID = "cn-hangzhou";

    /**
     * 描述: 阿里云发送短信接口
     *
     * @param phone            手机号
     * @param templateParamMap 发送短信的额外json参数, 目前需要 传 code，对应验证码数字串
     * @param aliSmsEnum       阿里账号配置的签名和模板
     * @return boolean
     * @author fan
     * @date 2020-06-23 16:13
     */
    public static String sendSms(String phone, Map<String, Object> templateParamMap, AliSmsEnum aliSmsEnum) {
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, ACCESS_KEY_ID, ACCESS_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(DOMAIN);
        request.setSysVersion(COMMON_REQUEST_VERSION);
        request.setSysAction(COMMON_REQUEST_ACTION);
        request.putQueryParameter("RegionId", REGION_ID);
        //手机号
        request.putQueryParameter("PhoneNumbers", phone);
        //短信签名名称
        request.putQueryParameter("SignName", aliSmsEnum.getSignName());
        //短信模板ID
        request.putQueryParameter("TemplateCode", aliSmsEnum.getTemplateCode());
        //短信模板变量对应的实际值，JSON格式。
        request.putQueryParameter("TemplateParam", JsonUtil.toString(templateParamMap));
//        request.putQueryParameter("OutId", "啊啊");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response);
            System.out.println("===============");
            System.out.println(response.getData());
            Map jsonObject = JsonUtil.parse(response.getData(), Map.class);
            String resCodeValue = "OK";
            String resCodeKey = "Code";
            if (resCodeValue.equals(jsonObject.get(resCodeKey))) {
                return "成功";
            }
            /*
                {"Message":"OK","RequestId":"8DCE9FEF-3717-4DF3-8463-D66384177A11","BizId":"332425092818573224^0","Code":"OK"}
              */
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("短信发送异常", e);
            System.out.println("短信发送异常" + e);
            return "短信发送异常:" + e.getMessage() + ",是什么情况:" + e.toString();
        }
        return "没成功";
    }

    /**
     * 描述: 阿里云发送短信接口
     *
     * @param phone    手机号
     * @param authCode 验证码随机数字串
     * @return boolean
     * @author fan
     * @date 2020-06-23 16:13
     */
    public static String sendSms(String phone, String authCode) {
        //拼接验证码发送的json字符串
        Map<String, Object> jsonParam = new HashMap<>();
        jsonParam.put("code", authCode);
        return sendSms(phone, jsonParam, AliSmsEnum.LOGIN_TEMPLATE_CODE);
    }

    /**
     * 描述: 阿里云发送短信接口
     *
     * @param phone    手机号
     * @param authCode 验证码随机数字串
     * @return boolean
     * @author fan
     * @date 2020-06-23 16:13
     */
    public static String sendSms(String phone, String authCode, AliSmsEnum aliSmsEnum) {
        //拼接验证码发送的json字符串
        Map<String, Object> jsonParam = new HashMap<>();
        jsonParam.put("code", authCode);
        return sendSms(phone, jsonParam, aliSmsEnum);
    }
}
