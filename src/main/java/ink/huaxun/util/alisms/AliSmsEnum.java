package ink.huaxun.util.alisms;

import org.apache.commons.lang3.StringUtils;

/**
 * @author joe
 */

public enum AliSmsEnum {
    /**
     * 描述:
     *
     * @return
     * @author fan
     * @date 2020-06-22 17:15
     */
    LOGIN_TEMPLATE_CODE("SMS_193506988", "今余OA办公", "code"),
    /**
     * 描述:
     *
     * @return
     * @author fan
     * @date 2020-06-22 17:15
     */
    FORGET_PASSWORD_TEMPLATE_CODE("SMS_193506988", "今余OA办公", "code"),
    /**
     * 描述:
     *
     * @return
     * @author fan
     * @date 2020-06-22 17:15
     */
    REGISTER_TEMPLATE_CODE("SMS_193506988", "今余OA办公", "code");

    /**
     * 短信模板编码
     */
    private String templateCode;
    /**
     * 签名
     */
    private String signName;
    /**
     * 短信模板必需的数据名称，多个key以逗号分隔，此处配置作为校验
     */
    private String keys;

    AliSmsEnum(String templateCode, String signName, String keys) {
        this.templateCode = templateCode;
        this.signName = signName;
        this.keys = keys;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public static AliSmsEnum toEnum(String templateCode) {
        if (StringUtils.isEmpty(templateCode)) {
            return null;
        }
        for (AliSmsEnum item : AliSmsEnum.values()) {
            if (item.getTemplateCode().equals(templateCode)) {
                return item;
            }
        }
        return null;
    }
}
