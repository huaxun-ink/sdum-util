package ink.huaxun.util;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.lang.NonNull;

import java.text.ParseException;
import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * @author zhaogang
 * @description 身份证校验（CRC16）工具类
 * @date 2023/3/15 13:54
 */
public class IdCardUtil {

    // 最小年份
    private static final int MIN_YEAR = 1800;

    // 年份（日期）开始位
    private static final int YEAR_DATE_START_INDEX = 6;

    // 年份结束位
    private static final int YEAR_END_INDEX = 10;

    // 日期结束位
    private static final int DATE_END_INDEX = 14;

    // 日期格式
    private static final String DATE_PATTERN = "yyyyMMdd";

    // 校验开始位
    private static final int CHECK_START_INDEX = 17;

    // 校验结束位
    private static final int CHECK_END_INDEX = 18;

    // 加权系数
    private static final int[] COEFFICIENT = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

    // 校验码对照表
    private static final String[] LAST = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2", "1"};

    // 身份证正则
    private static final String PATTERN = "^\\d{17}(\\d|[X|x])$";

    /**
     * @author zhaogang
     * @description 校验身份证有效性
     * @date 2021-12-01 10:33
     */
    public static boolean check(@NonNull String idCard) {
        return check(idCard, true);
    }

    /**
     * @author zhaogang
     * @description 校验身份证有效性
     * @date 2021-12-01 10:33
     */
    public static boolean check(@NonNull String idCard, boolean strict) {
        if (!Pattern.matches(PATTERN, idCard)) {
            return false;
        }
        int year = Integer.parseInt(idCard.substring(YEAR_DATE_START_INDEX, YEAR_END_INDEX));
        if (year < MIN_YEAR || year > Calendar.getInstance().get(Calendar.YEAR)) {
            return false;
        }
        // 严格模式
        if (strict) {
            try {
                DateUtils.parseDateStrictly(idCard.substring(YEAR_DATE_START_INDEX, DATE_END_INDEX), DATE_PATTERN);
            } catch (ParseException ignore) {
                return false;
            }
        }
        return idCard.substring(CHECK_START_INDEX, CHECK_END_INDEX).toUpperCase().equals(calculateLast(idCard));
    }

    /**
     * @author zhaogang
     * @description 根据加权系数取余计算第18位
     * @date 2021-12-01 10:48
     */
    private static String calculateLast(@NonNull String idCard) {
        int sum = 0;
        for (int i = 0; i < COEFFICIENT.length; i++) {
            sum += Character.digit(idCard.charAt(i), 10) * COEFFICIENT[i];
        }
        return LAST[sum % 11];
    }

}
