package ink.huaxun.util;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * @author zhaogang
 * @description 生成图片验证码
 * @date 2023/6/13 10:00
 */
public class ImageCaptchaUtil {

    /**
     * 指定图片的宽度
     */
    private static final int WIDTH = 200;

    /**
     * 指定图片的高度
     */
    private static final int HEIGHT = 40;

    /**
     * 干扰线的长度=1.414*lineWidth
     */
    private static final int LINE_WIDTH = 2;

    /**
     * 图片格式
     */
    private static final String IMG_FORMAT = "JPEG";

    /**
     * base64图片前缀
     */
    private static final String BASE64_PREFIX = "data:image/jpg;base64,";

    /**
     * 定义干扰线数量
     */
    private static final int COUNT = 200;

    /**
     * 生成验证码
     */
    public static String generate(String code) {
        BufferedImage image = getImageBuffer(code);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, IMG_FORMAT, byteStream);
        } catch (IOException e) {
            return StringUtils.EMPTY;
        }
        byte[] bytes = byteStream.toByteArray();
        String base64 = Base64.getEncoder().encodeToString(bytes).trim();
        return BASE64_PREFIX + base64.replaceAll("\n", "").replaceAll("\r", "");
    }

    /**
     * 生成图片缓存
     */
    private static BufferedImage getImageBuffer(String code) {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);
        graphics.drawRect(0, 0, WIDTH - 1, HEIGHT - 1);

        // 随机产生干扰线，使图象中的认证码不易被其它程序探测到
        for (int i = 0; i < COUNT; i++) {
            graphics.setColor(getRandomColor());
            // 保证画在边框之内
            int x = RandomUtils.nextInt(1, WIDTH - LINE_WIDTH - 1);
            int y = RandomUtils.nextInt(1, HEIGHT - LINE_WIDTH - 1);
            graphics.drawLine(x, y, x + RandomUtils.nextInt(0, LINE_WIDTH), y + RandomUtils.nextInt(0, LINE_WIDTH));
        }

        // 向图片生成code
        for (int i = 0; i < code.length(); i++) {
            graphics.setColor(Color.BLACK);
            graphics.setFont(new Font("Times New Roman", Font.BOLD, 24));
            graphics.drawString(String.valueOf(code.charAt(i)), (23 * i) + 8, 26);
        }
        graphics.dispose();
        return image;
    }

    /**
     * 取得给定范围随机颜色
     */
    private static Color getRandomColor() {
        int r = RandomUtils.nextInt(100, 200);
        int g = RandomUtils.nextInt(100, 200);
        int b = RandomUtils.nextInt(100, 200);
        return new Color(r, g, b);
    }

}
