package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaogang
 * @description http工具类
 * @date 2023/3/8 11:14
 */
public class HttpUtil {

    /**
     * token的头
     */
    private static final String HEADER = SpringContextUtil.getBean(Environment.class).getProperty("token.header", "");

    private static final int MAX_IN_MEMORY_SIZE = 10 * 1024 * 1024;

    private static final WebClient WEB_CLIENT = WebClient.builder()
            .codecs(config -> config.defaultCodecs().maxInMemorySize(MAX_IN_MEMORY_SIZE)).build();

    public static WebClient getWebClient() {
        return WEB_CLIENT;
    }

    public static String get(String url) {
        return get(url, StringUtils.EMPTY);
    }

    public static String get(String url, String token) {
        return get(url, token, new HashMap<>(0));
    }

    public static String get(String url, Map<String, Object> params) {
        return get(url, null, params);
    }

    public static String get(String url, String token, Map<String, Object> params) {
        return WEB_CLIENT
                .get().uri(url, params).header(HEADER, token)
                .exchangeToMono(response -> response.bodyToMono(String.class))
                .block();
    }

    public static String post(String url) {
        return post(url, StringUtils.EMPTY);
    }

    public static String post(String url, String json) {
        return post(url, null, json);
    }

    public static String post(String url, String token, String json) {
        return WEB_CLIENT
                .post().uri(url).header(HEADER, token)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json)
                .exchangeToMono(response -> response.bodyToMono(String.class))
                .block();
    }

    public static String put(String url) {
        return put(url, StringUtils.EMPTY);
    }

    public static String put(String url, String json) {
        return put(url, null, json);
    }

    public static String put(String url, String token, String json) {
        return WEB_CLIENT
                .put().uri(url).header(HEADER, token)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json)
                .exchangeToMono(response -> response.bodyToMono(String.class))
                .block();
    }

    public static String delete(String url) {
        return delete(url, StringUtils.EMPTY);
    }

    public static String delete(String url, String token) {
        return delete(url, token, new HashMap<>(0));
    }

    public static String delete(String url, Map<String, Object> params) {
        return delete(url, null, params);
    }

    public static String delete(String url, String token, Map<String, Object> params) {
        return WEB_CLIENT
                .delete().uri(url, params).header(HEADER, token)
                .exchangeToMono(response -> response.bodyToMono(String.class))
                .block();
    }

}
