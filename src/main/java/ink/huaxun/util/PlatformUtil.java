package ink.huaxun.util;

/**
 * @author zhaogang
 * @description 平台工具类
 * @date 2023/2/7 15:23
 */
public class PlatformUtil {

    /**
     * jar标志
     */
    private static final String JAR_SYMBOL = "BOOT-INF/lib";

    /**
     * 判断是否在服务器运行（jar模式启动）
     */
    public static boolean server() {
        String path = PlatformUtil.class.getResource("PlatformUtil.class").getPath();
        return path.contains(JAR_SYMBOL);
    }

    /**
     * 判断是否在开发环境运行（ide模式启动）
     */
    public static boolean develop() {
        return !server();
    }

}
