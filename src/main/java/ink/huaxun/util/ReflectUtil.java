package ink.huaxun.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author zhaogang
 * @description 反射工具类
 * @date 2023/3/8 15:27
 */
public class ReflectUtil {

    public static <T> T newInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (ReflectiveOperationException ignored) {
            return null;
        }
    }

    /**
     * 根据方法名称反射获取方法
     */
    public static Method getMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
        try {
            return clazz.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException ignored) {
            return null;
        }
    }

    /**
     * 执行方法获取返回值
     */
    public static Object invoke(Object entity, Method method, Object... params) {
        try {
            return method.invoke(entity, params);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ignored) {
            return null;
        }
    }

}
