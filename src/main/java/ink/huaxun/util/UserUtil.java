package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhaogang
 * @description 用户工具类
 * @date 2021-06-07 17:15
 */
public class UserUtil {

    /**
     * token的头
     */
    private static final String HEADER = SpringContextUtil.getBean(Environment.class).getProperty("token.header", "");

    /**
     * 默认用户ID
     */
    private static final Long DEFAULT_USER_ID = 0L;

    /**
     * 获取用户ID
     */
    public static Long getId() {
        String token = UserUtil.getToken();
        String userId = JwtUtil.getId(token);
        if (StringUtils.isBlank(userId)) {
            return DEFAULT_USER_ID;
        }
        return Long.valueOf(userId);
    }

    /**
     * 从header获取token
     */
    public static String getToken() {
        HttpServletRequest request = HttpServletUtil.getRequest();
        if (request == null) {
            return null;
        }
        return request.getHeader(HEADER);
    }

}
