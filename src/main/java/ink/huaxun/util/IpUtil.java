package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhubo
 * @description ip工具类
 * @date 2023/6/13 11:08
 */
public class IpUtil {

    private static final String UNKNOWN = "unknown";

    /**
     * 获取IP地址
     * 使用nginx等反向代理软件， 则不能通过request.getRemoteAddr()获取IP地址
     * 如果使用了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP地址，X-Forwarded-For中第一个非unknown的有效IP字符串，则为真实IP地址
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (isEffective(ip)) {
            return ip;
        }

        ip = request.getHeader("Proxy-Client-IP");
        if (isEffective(ip)) {
            return ip;
        }

        ip = request.getHeader("WL-Proxy-Client-IP");
        if (isEffective(ip)) {
            return ip;
        }

        ip = request.getHeader("HTTP_CLIENT_IP");
        if (isEffective(ip)) {
            return ip;
        }

        ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        if (isEffective(ip)) {
            return ip;
        }

        return request.getRemoteAddr();
    }

    /**
     * 校验ip是否合法
     */
    private static boolean isEffective(String ip) {
        return StringUtils.isNotBlank(ip) && !UNKNOWN.equalsIgnoreCase(ip);
    }

}
