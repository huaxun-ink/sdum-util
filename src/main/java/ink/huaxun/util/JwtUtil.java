package ink.huaxun.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Payload;
import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author zhaogang
 * @description token操作工具类
 * @date 2021-06-07 17:16
 */
public class JwtUtil {

    /**
     * token秘钥
     */
    private static final String SECRET = "%e7%8e%8b%e6%80%9d%e6%b6%b5";

    /**
     * token过期时间
     */
    private static final int CALENDAR_FIELD = Calendar.MINUTE;

    private static final int CALENDAR_INTERVAL = 30;

    /**
     * token签发方
     */
    private static final String ISSUE = "admin";

    /**
     * 签发token
     * @author zhaogang
     * @date 2020-05-27 9:14
     */
    public static String createToken(String id, String... audience) {
        // 不予签发Token
        if (StringUtils.isBlank(id)) {
            return null;
        }
        Calendar now = Calendar.getInstance();
        // 签发时间
        Date issueTime = now.getTime();
        now.add(CALENDAR_FIELD, CALENDAR_INTERVAL);
        // 过期时间
        Date expireTime = now.getTime();

        // 生成Token
        return JWT.create()
                .withIssuer(ISSUE)
                .withAudience(audience)
                .withSubject(id)
                .withIssuedAt(issueTime)
                .withExpiresAt(expireTime)
                .sign(Algorithm.HMAC256(SECRET));
    }

    /**
     * 解密token获取decoded
     * @author zhaogang
     * @date 2020-05-27 9:22
     */
    private static DecodedJWT getDecoded(String token) {
        if (StringUtils.isBlank(token)) {
            return null;
        }
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
        try {
            return verifier.verify(token);
        } catch (JWTVerificationException error) {
            return null;
        }
    }

    /**
     * 获取用户id
     * @author zhaogang
     * @date 2020-05-27 9:22
     */
    public static String getId(String token) {
        Payload payload = getDecoded(token);
        if (payload == null) {
            return null;
        }
        return payload.getSubject();
    }

}
