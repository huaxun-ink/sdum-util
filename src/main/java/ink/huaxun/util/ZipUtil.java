package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author zhaogang
 * @description 压缩工具类
 * @date 2020-11-09 12:12
 */
public class ZipUtil {

    /**
     * 压缩文件后缀
     */
    public static final String SUFFIX = ".zip";

    public static void folderToZip(String sourcePath, String zipPath, String fileName) {
        File sourceFile = new File(sourcePath);

        if (!sourceFile.mkdirs() && !sourceFile.exists()) {
            return;
        }
        File zipFile = new File(zipPath + File.separator + fileName + SUFFIX);
        try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)))) {
            writeZip(sourceFile, StringUtils.EMPTY, zos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeZip(File file, String parentPath, ZipOutputStream zos) {
        if (file.isDirectory()) {
            //目录
            parentPath += file.getName() + File.separator;
            File[] files = file.listFiles();
            if (files == null) {
                return;
            }
            for (File f : files) {
                writeZip(f, parentPath, zos);
            }
            return;
        }
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            //指定zip文件夹
            ZipEntry zipEntry = new ZipEntry(parentPath + file.getName());
            zos.putNextEntry(zipEntry);
            int len;
            byte[] buffer = new byte[1024 * 10];
            while ((len = bis.read(buffer, 0, buffer.length)) != -1) {
                zos.write(buffer, 0, len);
                zos.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}