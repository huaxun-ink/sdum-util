package ink.huaxun.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 脱敏工具类
 *
 * @author zhubo
 */
public class DataDesUtil {

    private static final Integer DEFAULT_MOBILE_FRONT_SIZE = 3;

    private static final Integer DEFAULT_MOBILE_BACK_SIZE = 4;

    private static final Integer DEFAULT_MOBILE_LENGTH = 11;

    private static final Integer DEFAULT_ID_CARD_FRONT_LENGTH = 6;

    private static final Integer DEFAULT_ID_CARD_BACK_LENGTH = 4;

    private static final Integer DEFAULT_ID_CARD_LENGTH = 18;

    /**
     * 手机号码前三后四脱敏
     *
     * @param mobile 手机号
     * @return 脱敏后手机号
     */
    public static String mobile(String mobile) {
        return mobile(mobile, DEFAULT_MOBILE_FRONT_SIZE, DEFAULT_MOBILE_BACK_SIZE);
    }

    /**
     * 手机号码脱敏（一般是11位）
     *
     * @param mobile    手机号
     * @param frontSize 前部可以放出来长度
     * @param backSize  后部放出来的长度
     * @return 脱敏后手机号
     */
    public static String mobile(String mobile, int frontSize, int backSize) {
        if (StringUtils.isBlank(mobile) || (mobile.length() != DEFAULT_MOBILE_LENGTH)) {
            return mobile;
        }
        if (frontSize < 0 || backSize < 0) {
            throw new IllegalStateException("输入参数错误");
        }
        if (frontSize + backSize > DEFAULT_MOBILE_LENGTH) {
            throw new IllegalStateException("输入参数超过手机号长度");
        }
        int hideSize = DEFAULT_MOBILE_LENGTH - frontSize - backSize;
        String regex = "(\\d{" + frontSize + "})\\d{" + hideSize + "}(\\d{" + backSize + "})";
        return mobile.replaceAll(regex, "$1****$2");
    }

    /**
     * 默认身份证脱敏方法（前6后4）
     * 身份证脱敏
     *
     * @param id 身份证号
     * @return 脱敏后身份证号
     */
    public static String idCard(String id) {
        return idCard(id, DEFAULT_ID_CARD_FRONT_LENGTH, DEFAULT_ID_CARD_BACK_LENGTH);
    }

    /**
     * @param id        身份证号
     * @param frontSize 前段可以放出来长度
     * @param backSize  后段可以放出来长度
     * @return 脱敏后身份证号
     */
    public static String idCard(String id, int frontSize, int backSize) {
        if (StringUtils.isBlank(id) || (id.length() != DEFAULT_ID_CARD_LENGTH)) {
            return id;
        }
        if (frontSize < 0 || backSize < 0) {
            throw new IllegalStateException("输入参数错误");
        }
        if (frontSize + backSize > DEFAULT_ID_CARD_LENGTH) {
            throw new IllegalStateException("输入参数超过身份证号长度");
        }
        String regex = "(?<=\\w{" + frontSize + "})\\w(?=\\w{" + backSize + "})";
        return id.replaceAll(regex, "*");
    }

    /**
     * [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     *
     * @param fullName 姓名
     * @return 脱敏
     */
    public static String chineseNameLeft(String fullName) {
        if (StringUtils.isBlank(fullName)) {
            return fullName;
        }
        return chineseName(fullName, false, 1);
    }

    /**
     * [中文姓名] 只显示最后一个汉字，其他隐藏为2个星号<例子：**丹>
     *
     * @param fullName 姓名
     * @return 脱敏
     */
    public static String chineseNameRight(String fullName) {
        return chineseName(fullName, true, 1);
    }

    /**
     * 中文姓名脱敏
     *
     * @param name   姓名
     * @param left   是否从左侧脱敏 true-是 false-否
     * @param length 保留长度
     * @return 脱敏姓名
     */
    public static String chineseName(String name, Boolean left, int length) {
        if (StringUtils.isBlank(name)) {
            return name;
        }
        if (length > name.length()) {
            throw new IllegalStateException("脱敏长度不可超过姓名长度");
        }
        if (left == null) {
            throw new IllegalStateException("脱敏参数错误");
        }
        if (left) {
            return StringUtils.leftPad(StringUtils.right(name, name.length() - length), StringUtils.length(name), "*");
        }
        return StringUtils.rightPad(StringUtils.left(name, length), StringUtils.length(name), "*");
    }

    /**
     * 只显示到地区，不显示详细地址; <例子：北京市海淀区****>
     *
     * @param address 地址（需要包含区）
     * @return 脱敏地址
     */
    public static String address(String address) {
        String[] temp = StringUtils.split(address, "区");
        if (temp.length <= 1) {
            throw new IllegalStateException("地址内容不符合要求");
        }
        int length = temp[temp.length - 1].length();
        return address(address, address.length() - length);
    }

    /**
     * 地址脱敏
     *
     * @param address 地址
     * @param length  保留信息长度
     * @return 脱敏地址
     */
    public static String address(String address, int length) {
        if (StringUtils.isBlank(address)) {
            return address;
        }
        return StringUtils.rightPad(StringUtils.left(address, length), address.length(), "*");
    }

    /**
     * [电子邮箱] 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示<例子:g**@163.com>
     *
     * @param email 邮箱
     * @return 脱敏邮箱
     */
    public static String email(String email) {
        return email(email, 1);
    }

    /**
     * @param email  电子邮箱
     * @param length 保留长度
     * @return 脱敏邮箱
     */
    public static String email(String email, int length) {
        if (StringUtils.isBlank(email)) {
            return email;
        }
        int index = StringUtils.indexOf(email, "@");
        if (index <= 1) {
            throw new IllegalStateException("邮箱格式错误");
        }
        if (length > index) {
            throw new IllegalStateException("脱敏长度不可超过邮箱前缀");
        }
        return StringUtils.rightPad(StringUtils.left(email, length), index, "*").concat(StringUtils.mid(email, index, StringUtils.length(email)));

    }

    /**
     * [银行卡号] 前六位，后四位，其他用星号隐藏每位1个星号<例子:6222600**********1234>
     *
     * @param cardNum 银行卡号
     * @return 脱敏银行卡号
     */
    public static String bankCard(String cardNum) {
        if (StringUtils.isBlank(cardNum)) {
            return cardNum;
        }
        return StringUtils.left(cardNum, 6).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"), "******"));
    }

    /**
     * 左自定义脱敏（格式为：你好***********）
     *
     * @param value  字符串
     * @param length 保留信息的长度
     * @return 脱敏后字符串
     */
    public static String sensitive(String value, int length) {
        if (StringUtils.isBlank(value)) {
            return value;
        }
        return StringUtils.rightPad(StringUtils.left(value, length), value.length(), "*");
    }

}
